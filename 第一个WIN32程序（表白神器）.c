/*****************************************************************************
*作者：易Tian		QQ：161690260
*
*创作日期：2018年7月18日
*
*修改日期:2018年7月19日
*
*编译环境：Visual Studio 2017
*
*程序名称：表白神器（我的第一个WIN32程序）
*
*版权所有：Zerone安全团队 - 易Tian
*
*团队官方群:		技术交流群：729300885
*				软件工程群：511398305
*				社会工程群：813572801
*
******************************************************************************/

//本程序总共耗费时间：  5  小时

#include <windows.h>

#define IDB_ONE 3301
#define IDB_TWO 3302

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int j = 0;
int yes = 0;

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	PSTR szCmdLine,
	int iCmdShow)
{
	static TCHAR szAppName[] = TEXT("ZeroneSecTeam");
	HWND hwnd;
	MSG msg;
	WNDCLASS wndclass;

	int scrWidth, scrHeight;
	RECT rect;


	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;



	if (!RegisterClass(&wndclass))
	{
		MessageBox(NULL, TEXT("这个程序需要在 Windows NT 才能执行！"), szAppName, MB_ICONERROR);
		return 0;
	}



	hwnd = CreateWindow(szAppName,
		TEXT("ZeroneSecTeam"),
		WS_OVERLAPPEDWINDOW,
		300,
		100,
		350,
		180,
		NULL,
		NULL,
		hInstance,
		NULL);

	//居中窗口
	scrWidth = GetSystemMetrics(SM_CXSCREEN);
	scrHeight = GetSystemMetrics(SM_CYSCREEN);
	GetWindowRect(hwnd, &rect);
	SetWindowPos(hwnd, HWND_TOPMOST, (scrWidth - rect.right) / 2, (scrHeight - rect.bottom) / 2, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;

}

LRESULT CALLBACK WndProc(HWND hwnd,
	UINT message,
	WPARAM wParam,
	LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rect;


	switch (message)
	{
	case WM_CREATE:
		//两个按钮
		CreateWindow(TEXT("button"),
			TEXT("好~"),
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			96, 60, 60, 26,
			hwnd, (HMENU)3301,
			((LPCREATESTRUCT)lParam)->hInstance, NULL);

		CreateWindow(TEXT("button"),
			TEXT("不好~"),
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			166, 60, 60, 26,
			hwnd, (HMENU)3302,
			((LPCREATESTRUCT)lParam)->hInstance, NULL);



		return 0;

	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
		case IDB_ONE:
			MessageBox(hwnd, TEXT("抱起来，么么哒~"), TEXT("我爱你"), MB_OK);
			yes = 1;		//如果同意，yes = 1，则可以关闭程序
			break;
		case IDB_TWO:
			switch (j)
			{
			case 0:
				MessageBox(hwnd, TEXT("保大~"), TEXT("考虑一下呗"), MB_OK);
				j++;
				break;
			case 1:
				MessageBox(hwnd, TEXT("工资全给你~"), TEXT("考虑一下呗"), MB_OK);
				j++;
				break;
			case 2:
				MessageBox(hwnd, TEXT("家务活我干~"), TEXT("考虑一下呗"), MB_OK);
				j++;
				break;
			case 3:
				MessageBox(hwnd, TEXT("让我向东绝不向西~"), TEXT("考虑一下呗"), MB_OK);
				j++;
				break;
			default:
				//想不出有什么好的词了，凑合来吧，哈哈
				MessageBox(hwnd, TEXT("我不管，你必须当我女朋友，否则别想退出~"), TEXT("考虑一下呗"), MB_OK);
				j++;
				break;

			}

			break;
		default:
			break;
		}


		return 0;


	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		DrawText(hdc, TEXT("可以做我女朋友吗？"), -1, &rect,
			DT_SINGLELINE | DT_CENTER);
		EndPaint(hwnd, &ps);
		return 0;

	case WM_LBUTTONUP:
		//娱乐，可屏蔽
		MessageBox(hwnd, TEXT("别想其他，快选择~"), TEXT("警告"), MB_OK);
		return 0;

	case WM_CLOSE:
		//判断是否同意
		if (yes == 0)
		{
			MessageBox(hwnd, TEXT("不同意是关不掉的~"), TEXT("你需要同意做我女朋友"), MB_OK);
			return 0;
		}
		else
			DestroyWindow(hwnd);


	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hwnd, message, wParam, lParam);

}